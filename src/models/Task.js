const defaultTask = { title: '', notes: '', completed: null }

export default class Task {
    constructor(json = defaultTask, ) {
        const {
            id = null,
            title,
            notes, 
            completed = null,
        } = json;
        
        this.id = id;
        this.title = title || '';
        this.notes = notes || '';
        this.completed = completed ? new Date(completed) : null;
    }
}