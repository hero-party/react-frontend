import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'

export default function TaskListItem(props) {
    const { item, editItem, deleteItem, toggleItemComplete } = props;
    return (
        <Container item={item}>
            <Check item={item} toggleItemComplete={toggleItemComplete}/>
            <Title item={item}/>
            <ButtonPanel item={item} editItem={editItem} deleteItem={deleteItem}/>
        </Container>
    );
}

function Container(props) {
    const { item, children } = props
    const className = "d-flex justify-content-between align-items-center"
    return <div key={item.id} className={className}>
        { children }
    </div>
}

function Check(props) {
    const { item, toggleItemComplete } = props
    return <Form.Check
        type='checkbox'
        className='checkbox-lg'
        checked={item.completed ? item.completed : false}
        onChange={() => toggleItemComplete(item)}
    />
}

function Title(props) {
    const { item } = props
    const doneComponent = item.completed ?"completed-todo": ""
    const className = `todo-title mx-2 flex-fill ${doneComponent}`
    const titleAttr = item.notes ? item.notes : item.title
    return <span className={className} title={titleAttr}>
        {item.title}
    </span>
}

function ButtonPanel(props) {
    const { item, editItem, deleteItem } = props;
    return (
        <div>
            <Button
                variant='secondary'
                onClick={() => editItem(item)}
                className='mr-2 p-2'
                style={{ height: '40px' }}
            >
                <FontAwesomeIcon
                    icon={faEdit}
                    pull='right'
                    style={{ 'marginBottom': '2px' }}
                />
            </Button>
            <Button
                variant='danger'
                onClick={() => deleteItem(item)}
                style={{ height: '40px' }}
            >
                <FontAwesomeIcon icon={faTrash} />
            </Button>
        </div>
    )
}