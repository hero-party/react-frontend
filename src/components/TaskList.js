import React, { Component } from 'react';
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';

import TasksQueryOptionsControl, { COMPLETED_FILTER, SORT_FIELD_OPTION } from './TasksQueryOptionsControl'
import Modal from './TaskEditModal';
import TaskListItem from './TaskListItem';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import Task from '../models/Task';

export default class TaskList extends Component {
    constructor(props) {
        super(props);
        this.editTaskModal = React.createRef();
        this.taskListURL = props.taskListURL
    }
    
    state = {
        modal: false,
        todoList: [],
        query: {
            completedFilter: COMPLETED_FILTER.INCOMPLETE,
            sortField: SORT_FIELD_OPTION.TITLE,
        },
    };

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios
            .get(this.taskListURL)
            .then(response => {
                const todoList = response.data.map(taskData => new Task(taskData));
                this.setState({ todoList });
            })
            .catch(this.handleFailure);
    };

    setFilter = mode => {
        this.setState({ filter: mode });
    };

    toggle = () => {
        this.setState((prevState, props) => ({ modal: !prevState.modal }));
    };

    submitSuccess = () => {
        this.refreshList();
    };

    handleFailure = task => error => {
        alert(`${task} failed due to error: ${error}`);
        this.refreshList();
    }

    handleSubmit = item => {
        this.toggle();

        const isUpdate = item.id !== null;
        const task = isUpdate ? 'Updating' : 'Creating';

        if (isUpdate) {
            axios
                .put(`${this.taskListURL}${item.id}/`, item)
                .then(this.submitSuccess)
                .catch(this.handleFailure(task));
        } else {
            axios
                .post(this.taskListURL, item)
                .then(this.submitSuccess)
                .catch(this.handleFailure(task));
        }
    }
    
    toggleItemComplete = item => {
        const newItem = { ...item, completed: !item.completed };
        this.handleSubmit(newItem);
    }

    handleDelete = item => {
        axios
            .delete(`/api/tasks/${item.id}/`)
            .then(this.submitSuccess)
            .catch(this.handleFailure('Deleting'));
    }

    createItem = () => {
        this.editTaskModal.current.presentTask();
    }

    editItem = item => {
        this.editTaskModal.current.presentTask(item);
    }
    
    renderList() {
        const { completedFilter, sortField } = this.state.query;
        const items = this.state.todoList
            .filter(item => {
                switch (completedFilter) {
                    case COMPLETED_FILTER.COMPLETED:
                        return item.completed
                    case COMPLETED_FILTER.INCOMPLETE:
                        return !item.completed
                    case COMPLETED_FILTER.ALL:
                    default:
                        return true;
                }
            })
            .sort((item1, item2) => {
                switch(sortField) {
                    case SORT_FIELD_OPTION.TITLE:
                        return item1.title.localeCompare(item2.title);
                    case SORT_FIELD_OPTION.COMPLETED:
                    default:
                        // we want ASC sort order, so we subtract item1 from item2 here
                        return item2.completed - item1.completed;
                }
            });
        
        return (
            <ListGroup variant='flush'>
                {items.map(item => (
                    <ListGroupItem key={item.id}>
                        <TaskListItem
                            item={item}
                            editItem={this.editItem}
                            deleteItem={this.handleDelete}
                            toggleItemComplete={this.toggleItemComplete}
                        />
                    </ListGroupItem>
                ))}
            </ListGroup>
        );
    }
    
    render() {
        return (
            <Container className='pt-2 pt-md-3 pt-lg-5'>
                { this.taskListURL }
                <Row>
                    <Col md='6' sm='10' className='mx-auto p-0'>
                        <Card>
                            <Card.Header className='d-flex align-items-center'>
                                <span style={{fontSize: '2rem'}}>Tasks</span>
                                <Button onClick={this.createItem} className="ml-2">
                                    <FontAwesomeIcon icon={faPlus}/>
                                </Button>
                            </Card.Header>
                            <TasksQueryOptionsControl
                                query={this.state.query}
                                setQuery={query => this.setState({query})}
                            />
                            {this.renderList()}
                        </Card>
                    </Col>
                </Row>
                <Modal
                    ref={this.editTaskModal}
                    onHide={this.toggle}
                    onSave={this.handleSubmit}
                />
            </Container>
        );
    }
}