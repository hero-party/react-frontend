import React from 'react';
import Navbar from 'react-bootstrap/Navbar';

export default function NavigationBar() {
    return <Navbar expand="lg" variant='dark' bg='dark'>
        <Navbar.Brand>
            Hero Party
        </Navbar.Brand>
    </Navbar>
}