import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

import Task from '../models/Task';

export default class TaskEditModal extends Component {
    state = {
        show: false,
        task: new Task(),
    }
    
    presentTask = task => {
        if (!task) {
            task = new Task();
        }
        
        this.setState({show: true, task: task});
    }
    
    save = () => {
        this.props.onSave(this.state.task);
        this.hide();
    }
    
    hide = () => {
        this.setState({show: false, task: new Task()});
        this.props.onHide();
    }
    
    handleChange = e => {
        let { name, value } = e.target;
        this.setState((prevState, props) => {
            const task = { ...prevState.task, [name]: value };
            return { task };
        });
    };
    
    render() {
        const { show, task } = this.state;
        return (
            <Modal show={show} onHide={this.hide}>
                <Modal.Header closeButton> Task </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId='formTaskTitle'>
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                type='text'
                                name='title'
                                value={task.title}
                                onChange={this.handleChange}
                                placeholder='Enter Task Title'
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Notes</Form.Label>
                            <Form.Control
                                as='textarea'
                                rows='3'
                                name='notes'
                                value={task.notes}
                                onChange={this.handleChange}
                                placeholder='Enter Task Notes'
                            />
                        </Form.Group>
                        {
                            task.completed ? 
                                <Form.Label>
                                    <span role='img' aria-label='checkbox'>
                                        ✅&nbsp;
                                    </span>
                                    Completed at&nbsp;
                                    <span className='text-info'>
                                        {task.completed.toLocaleString()}
                                    </span>
                                </Form.Label>
                                : null
                        }
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button color='success' onClick={this.save}>
                        Save
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
