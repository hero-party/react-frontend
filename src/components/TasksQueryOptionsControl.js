import React, { Component } from 'react';

import Nav from 'react-bootstrap/Nav';
import { NavDropdown } from 'react-bootstrap';

export const COMPLETED_FILTER = {
    ALL: 'All',
    COMPLETED: 'Completed',
    INCOMPLETE: 'Incomplete',
};

export const SORT_FIELD_OPTION = {
    TITLE: 'Title',
    COMPLETED: 'Completed',
};

export default class TasksQueryOptionsControl extends Component {
    
    setCompletedFilter = completedFilter => {
        const { query, setQuery } = this.props;
        
        query.completedFilter = completedFilter;
        
        if (completedFilter === COMPLETED_FILTER.INCOMPLETE && query.sortField === SORT_FIELD_OPTION.COMPLETED) {
            query.sortField = SORT_FIELD_OPTION.TITLE;
        }
        
        setQuery(query);
    }
    
    setSortField = sortField => {
        const { query, setQuery } = this.props;
        query.sortField = sortField;
        setQuery(query);
    }
    
    render() {
        const { query } = this.props;
        
        const completedFilterOptions = Object.values(COMPLETED_FILTER);
        var sortFieldOptions = Object.values(SORT_FIELD_OPTION);
        
        if (query.completedFilter === COMPLETED_FILTER.INCOMPLETE) {
            sortFieldOptions = sortFieldOptions.filter(option => option !== SORT_FIELD_OPTION.COMPLETED);
        }
        
        return (
            <Nav variant='pills' className='m-2'>
                { 
                    completedFilterOptions.map(completedFilter =>
                        <Nav.Link
                            key={completedFilter}
                            onClick={() => this.setCompletedFilter(completedFilter)}
                            className={query.completedFilter === completedFilter ? "active" : ""}
                        >
                            {completedFilter}
                        </Nav.Link>
                    )
                }
                <NavDropdown
                    title={`Sort: ${query.sortField}`}
                    disabled={sortFieldOptions.length === 1}
                    className='ml-auto'
                >
                    {
                        sortFieldOptions.map(sortField => 
                            <NavDropdown.Item
                                key={sortField}
                                onClick={() => this.setSortField(sortField)}
                                className={query.sortField === sortField ? 'active' : ''}
                            >
                                {sortField}
                            </NavDropdown.Item>
                        )
                    }   
                </NavDropdown>
            </Nav>
        )
    }
}
