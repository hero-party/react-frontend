import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row  from 'react-bootstrap/Row';
import Col  from 'react-bootstrap/Col';

import NavigationBar from './components/NavigationBar';
import TaskList from './components/TaskList';
import TaskListURL from './data/TaskListURL'

class App extends Component {
    render() {
        return (
            <div>
                <NavigationBar/>
                <Container as='main'>
                    <Row>
                        <Col>
                            <TaskList taskListURL={TaskListURL}/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default App;